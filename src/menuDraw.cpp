#include"menuDraw.h"

namespace menuDr {

	void dr(database Data) {

		DrawTexture(Data.image, 0, 0, RAYWHITE);

		DrawText("ENDLESS", 450, 50, 80, WHITE);
		DrawText("PLAY", 600, 200, 35, WHITE);
		DrawText("CONTROLS", 570, 300, 35, WHITE);
		DrawText("EXIT", 600, 400, 35, WHITE);

		switch (Data.menuOption) {
		case 0:
			DrawLine(540, 235, 760, 235, WHITE);
			break;
		case 1:
			DrawLine(540, 335, 760, 335, WHITE);
			break;
		case 2:
			DrawLine(540, 435, 760, 435, WHITE);
			break;
		}
	}

	void optDraw(database Data) {

		DrawTexture(Data.image, 0, 0, RAYWHITE);

		DrawText("CONTROLS", 20, 1, 50, WHITE);
		DrawLine(10, 45, 1260, 45, WHITE);

		DrawText("UP:", 30, 80, 45, WHITE);
		DrawText("W", 120, 80, 45, WHITE);
		
		DrawText("DOWN:", 30, 200, 45, WHITE);
		DrawText("S", 190, 200, 45, WHITE);
		
		DrawText("LEFT:", 30, 320, 45, WHITE);
		DrawText("A", 190, 320, 45, WHITE);
		
		DrawText("RIGHT:", 30, 440, 45, WHITE);
		DrawText("D", 190, 440, 45, WHITE);

		DrawText("JUMP:", 30, 560, 45, WHITE);
		DrawText("SPACE", 190, 560, 45, WHITE);

		DrawText("PAUSE:", 600, 80, 45, WHITE);
		DrawText("P", 780, 80, 45, WHITE);

		DrawLine(10, 660, 1260, 660, WHITE);
		DrawText("BACK", 1150, 668, 35, WHITE);

		switch (Data.optionSwitch) {
		case 0:

			break;
		case 1:

			break;
		case 2:

			break;
		case 3:

			break;
		case 4:

			break;
		case 5:
			DrawLine(1130, 700, 1265, 700, WHITE);
			break;
		default:

			break;
		}
	}
}