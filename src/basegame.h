#ifndef BASEGAME_H
#define BASEGAME_H

#include"initWindow.h"
#include"upgrade.h"
#include"draw.h"

namespace bGame {
	void startGame();
}

#endif // !BASEGAME_H