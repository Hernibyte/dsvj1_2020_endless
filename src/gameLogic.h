#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include"incl.h"

namespace game {
	void gamelogic(database& Data);
	void timerLogic(database& Data);
	void controls(database& Data);

	void ManagerLevel(
		int& ObjectsInLevel, 
		object& normalObj, 
		float normalObjPosition, 
		object& dobleObj, 
		float dobleObjPosition, 
		object& colectableObj,
		int levelPosition);

	void ManagerCollision(
		player& _player, 
		object& normalObj, 
		object& doubleObj, 
		object& colectableObj, 
		int& points, 
		bool& isEndGame);

	void ResetLevelParameters(database& Data);
}

#endif // !GAMELOGIC_H