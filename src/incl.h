#ifndef INCL_H
#define INCL_H

#include<iostream>
#include"raylib.h"
#include<string>
#include<stdlib.h>
#include<time.h>

#define KEYS 5

struct player {
	Rectangle hitbox = { 80, 450, 30, 30 };
	int level = 2;
	int jumpState = 0;
};

struct object {
	Rectangle hitbox;
	int level;
};

struct database {
	Texture2D image;
	Texture2D esqueleto;
	Texture2D hueso;
	Texture2D personaje;
	Texture2D zombie;
	//----
	Sound MenuSound;
	bool MenuSActivate = true;
	Sound GameSound;
	bool GameSActivate = true;
	Sound SaltoSound;
	Sound GolpeSound;
	//-------------------
	int screenWidth = 1280;
	int screenHeight = 720;
	int setFPS = 60;
	const char* tittle = "ENDLESS MARATON";
	int caseOption = 0;
	int menuOption = 0;
	int optionSwitch = 0;
	bool iwn = true;
	bool checkKey = false;
	int GameKeys[KEYS] = { 87, 83, 65, 68, 32 };
	bool pause = false;
	bool endGame = false;
	int levelScale = 120;
	float timer[2];
	int points = 0;
	//-------------------
	int cantObjlvl1 = 3;
	int cantObjlvl2 = 3;
	int cantObjlvl3 = 3;
	//-------------------
	player Player;
	object Obj1;
	object obsDoble1;
	object obsColect1;
	object Obj2;
	object obsDoble2;
	object obsColect2;
	object Obj3;
	object obsDoble3;
	object obsColect3;
};

#endif // !INCL_H