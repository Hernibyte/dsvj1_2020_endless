#include"gameDraw.h"

namespace drawG {
	void drawGame(database Data) {

		if (!Data.pause && !Data.endGame) {

			DrawTexture(Data.image, 0, 0, RAYWHITE);

			DrawRectangle(0, 370, 1280, 10, RED);
			DrawRectangle(0, 490, 1280, 10, RED);
			DrawRectangle(0, 600, 1280, 10, RED);

			Vector2 playerpos = { Data.Player.hitbox.x, Data.Player.hitbox.y - 60 };
			DrawTextureEx(Data.personaje, playerpos, 1.0f, 0.26f, RAYWHITE);
			//---------------
			Vector2 zombie1pos = { Data.Obj1.hitbox.x, Data.Obj1.hitbox.y };
			DrawTextureEx(Data.zombie, zombie1pos, 1.0f, 0.1f, RAYWHITE);

			Vector2 esqueleto1pos = { Data.obsDoble1.hitbox.x, Data.obsDoble1.hitbox.y + 20 };
			DrawTextureEx(Data.esqueleto, esqueleto1pos, 1.0f, 0.26f, RAYWHITE);

			Vector2 hueso1pos = { Data.obsColect1.hitbox.x, Data.obsColect1.hitbox.y };
			DrawTextureEx(Data.hueso, hueso1pos, 1.0f, 0.1f, RAYWHITE);
			//------------
			Vector2 zombie2pos = { Data.Obj2.hitbox.x, Data.Obj2.hitbox.y };
			DrawTextureEx(Data.zombie, zombie2pos, 1.0f, 0.1f, RAYWHITE);

			Vector2 esqueleto2pos = { Data.obsDoble2.hitbox.x, Data.obsDoble2.hitbox.y + 20 };
			DrawTextureEx(Data.esqueleto, esqueleto2pos, 1.0f, 0.26f, RAYWHITE);

			Vector2 hueso2pos = { Data.obsColect2.hitbox.x, Data.obsColect2.hitbox.y };
			DrawTextureEx(Data.hueso, hueso2pos, 1.0f, 0.1f, RAYWHITE);
			//------------
			Vector2 zombie3pos = { Data.Obj3.hitbox.x, Data.Obj3.hitbox.y };
			DrawTextureEx(Data.zombie, zombie3pos, 1.0f, 0.1f, RAYWHITE);

			Vector2 esqueleto3pos = { Data.obsDoble3.hitbox.x, Data.obsDoble3.hitbox.y + 20 };
			DrawTextureEx(Data.esqueleto, esqueleto3pos, 1.0f, 0.26f, RAYWHITE);

			Vector2 hueso3pos = { Data.obsColect3.hitbox.x, Data.obsColect3.hitbox.y };
			DrawTextureEx(Data.hueso, hueso3pos, 1.0f, 0.1f, RAYWHITE);

			//-------
			DrawText(FormatText("Huesos: %i", Data.points), 1000, 30, 30, WHITE);

			//-------
			//DrawText(FormatText("Time: %i", Data.timer[0]), 600, 30, 30, RED);
			//DrawText(FormatText(": %i", Data.timer[1]), 700, 30, 30, RED);
		}
		else 
			if (Data.pause && !Data.endGame) {
				DrawTexture(Data.image, 0, 0, RAYWHITE);
				DrawText("PAUSE", 540, 200, 80, WHITE);
				DrawText("RESUME", 600, 360, 35, WHITE);
				DrawText("MENU", 620, 430, 35, WHITE);
			}

		if (Data.endGame) {
			DrawTexture(Data.image, 0, 0, RAYWHITE);
			DrawText(FormatText("HUESOS RECOLECTADOS: %i", Data.points), 460, 100, 30, WHITE);
			DrawText("YOU LOSE", 460, 200, 80, WHITE);
			DrawText("MENU", 600, 360, 35, WHITE);
		}
	}
}