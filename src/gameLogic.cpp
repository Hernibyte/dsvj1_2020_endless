#include"gameLogic.h"

namespace game {
	void gamelogic(database& Data) {
		srand(time(NULL));
		timerLogic(Data);

		if (Data.GameSActivate) {
			PlaySound(Data.GameSound);
			Data.GameSActivate = false;
		}
		if (!IsSoundPlaying(Data.GameSound)) {
			Data.GameSActivate = true;
		}

		if (!Data.pause && !Data.endGame) {
			if (!IsSoundPlaying(Data.GameSound)) {
				ResumeSound(Data.GameSound);
			}

			if (IsKeyPressed(KEY_P))
				Data.pause = true;

			controls(Data);

			ManagerLevel(Data.cantObjlvl1, Data.Obj1, 335, 
				Data.obsDoble1, 265, Data.obsColect1, 1);

			ManagerLevel(Data.cantObjlvl2, Data.Obj2, 455, 
				Data.obsDoble2, 385, Data.obsColect2, 2);

			ManagerLevel(Data.cantObjlvl3, Data.Obj3, 570, 
				Data.obsDoble3, 500, Data.obsColect3, 3);

			//managerCollision(Data);
			ManagerCollision(Data.Player, Data.Obj1, Data.obsDoble1, Data.obsColect1, Data.points, Data.endGame);
			ManagerCollision(Data.Player, Data.Obj2, Data.obsDoble2, Data.obsColect2, Data.points, Data.endGame);
			ManagerCollision(Data.Player, Data.Obj3, Data.obsDoble3, Data.obsColect3, Data.points, Data.endGame);
		}
		else
			if (Data.pause && !Data.endGame) {
				Rectangle btn_Resume = { 600, 360, 300, 60 };
				Rectangle btn_Menu = { 620, 430, 300, 60 };

				if (IsSoundPlaying(Data.GameSound)) {
					PauseSound(Data.GameSound);
				}
				if (IsKeyPressed(KEY_P))
					Data.pause = false;

				if (CheckCollisionPointRec(GetMousePosition(), btn_Resume)) {
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
						Data.pause = false;
					}
				}

				if (CheckCollisionPointRec(GetMousePosition(), btn_Menu)) {
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
						ResetLevelParameters(Data);
					}
				}
			}

		if (Data.endGame) {
			Rectangle button = { 600, 360, 300, 60 };

			if (CheckCollisionPointRec(GetMousePosition(), button)) {
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					ResetLevelParameters(Data);
				}
			}
		}
	}
	//---------------
	void timerLogic(database& Data) {
		Data.timer[1] += 0.016666f;
		if (Data.timer[1] >= 60.0f) { 
			Data.timer[1] = 0.0f;
			Data.timer[0] += 1.0f;
		}
	}
	//---------------
	void controls(database& Data) {
		float deltaTime = GetFrameTime();

		if (Data.Player.jumpState != 1) {
			if (IsKeyPressed(Data.GameKeys[0])) {
				if (Data.Player.level > 1) {
					Data.Player.level--;
					Data.Player.hitbox.y -= Data.levelScale;
				}
			}
			if (IsKeyPressed(Data.GameKeys[1])) {
				if (Data.Player.level < 3) {
					Data.Player.level++;
					Data.Player.hitbox.y += Data.levelScale;
				}
			}
		}
		// ----  LEFT
		if (IsKeyDown(Data.GameKeys[2])) 
			if (Data.Player.hitbox.x >= 2)	
					Data.Player.hitbox.x -= 800 * deltaTime;
		// ----  RIGHT
		if (IsKeyDown(Data.GameKeys[3]))
			if (Data.Player.hitbox.x <= 600) 
					Data.Player.hitbox.x += 800 * deltaTime;
		// ----  JUMP
		if (IsKeyPressed(Data.GameKeys[4])) {
			Data.Player.jumpState = 1;

			if (!IsSoundPlaying(Data.SaltoSound)) {
				PlaySound(Data.SaltoSound);
			}
		}
		// ---- INTERACT 
		if (IsKeyPressed(KEY_E)) {
			if (Data.Player.hitbox.x + 120 >= Data.Obj1.hitbox.x && 
				Data.Player.level == Data.Obj1.level) {
				PlaySound(Data.GolpeSound);
				Data.Obj1.hitbox.x = -30;
			}
			if (Data.Player.hitbox.x + 120 >= Data.Obj2.hitbox.x &&
				Data.Player.level == Data.Obj2.level) {
				PlaySound(Data.GolpeSound);
				Data.Obj2.hitbox.x = -30;
			}
			if (Data.Player.hitbox.x + 120 >= Data.Obj3.hitbox.x &&
				Data.Player.level == Data.Obj3.level) {
				PlaySound(Data.GolpeSound);
				Data.Obj3.hitbox.x = -30;
			}
		}

		if (Data.Player.jumpState == 1) {
			if (Data.Player.level == 1) {
				Data.Player.hitbox.y -= 200 * deltaTime;
				if (Data.Player.hitbox.y <= 220) {
					Data.Player.jumpState = 2;
				}
			}
			if (Data.Player.level == 2) {
				Data.Player.hitbox.y -= 200 * deltaTime;
				if (Data.Player.hitbox.y <= 340) {
					Data.Player.jumpState = 2;
				}
			}
			if (Data.Player.level == 3) {
				Data.Player.hitbox.y -= 200 * deltaTime;
				if (Data.Player.hitbox.y <= 460) {
					Data.Player.jumpState = 2;
				}
			}
		}
		else
			if (Data.Player.jumpState == 2) {
				if (Data.Player.level == 1) {
					Data.Player.hitbox.y += 200 * deltaTime;
					if (Data.Player.hitbox.y >= 330) {
						Data.Player.jumpState = 0;
					}
				}
				if (Data.Player.level == 2) {
					Data.Player.hitbox.y += 200 * deltaTime;
					if (Data.Player.hitbox.y >= 450) {
						Data.Player.jumpState = 0;
					}
				}
				if (Data.Player.level == 3) {
					Data.Player.hitbox.y += 200 * deltaTime;
					if (Data.Player.hitbox.y >= 570) {
						Data.Player.jumpState = 0;
					}
				}
			}
	}

	void ManagerLevel(int& ObjectsInLevel, object& normalObj, 
		float normalObjPosition, object& dobleObj, 
		float dobleObjPosition, object& colectableObj,
		int levelPosition)
	{
		int num = rand() % 3;
		if (ObjectsInLevel == 3) {
			normalObj.hitbox = { -100, normalObjPosition, 30, 30 };
			normalObj.level = levelPosition;

			dobleObj.hitbox = { -100, dobleObjPosition, 20, 80 };
			dobleObj.level = levelPosition;
			
			colectableObj.hitbox = { -100, normalObjPosition, 20, 20 };
			colectableObj.level = levelPosition;
			
			ObjectsInLevel++;
		}

		if (normalObj.hitbox.x <= -100 && num == 0)
			normalObj.hitbox.x = 1300;
		else
			normalObj.hitbox.x -= 600 * GetFrameTime();

		if (dobleObj.hitbox.x <= -100 && num == 1)
			dobleObj.hitbox.x = 1300;
		else
			dobleObj.hitbox.x -= 600 * GetFrameTime();

		if (colectableObj.hitbox.x <= -100 && num == 2)
			colectableObj.hitbox.x = 1300;
		else 
			colectableObj.hitbox.x -= 600 * GetFrameTime();
	}

	void ManagerCollision(player& _player, object& normalObj,
		object& doubleObj, object& colectableObj, 
		int& points, bool& isEndGame) 
	{
		bool checkCollObj;
		bool checkCollDouble;
		bool checkCollColectable;

		checkCollObj = CheckCollisionRecs(_player.hitbox, normalObj.hitbox);
		checkCollDouble = CheckCollisionRecs(_player.hitbox, doubleObj.hitbox);
		checkCollColectable = CheckCollisionRecs(_player.hitbox, colectableObj.hitbox);

		if (checkCollObj && _player.level == normalObj.level ||
			checkCollDouble && _player.level == doubleObj.level) {
			normalObj.hitbox.x = -30;
			doubleObj.hitbox.x = -30;
			isEndGame = true;
		}
		if (checkCollColectable && _player.level == colectableObj.level) {
			colectableObj.hitbox.x = -30;
			points++;
		}
	}
	
	void ResetLevelParameters(database& Data) {
		Data.caseOption = 0;
		Data.endGame = false;

		Data.points = 0;
		//-------------
		Data.Player.hitbox = { 80, 450, 30, 30 };
		Data.Player.level = 2;
		Data.Player.jumpState = 0;
		//-------------
		Data.Obj1.hitbox = { -47, 335, 30, 30 };
		Data.obsDoble1.hitbox = { -42, 265, 20, 80 };
		Data.obsColect1.hitbox = { -42, 335, 20, 20 };
		Data.Obj2.hitbox = { -54, 455, 30, 30 };
		Data.obsDoble2.hitbox = { -45, 385, 20, 80 };
		Data.obsColect2.hitbox = { -45, 455, 20, 20 };
		Data.Obj3.hitbox = { -51, 570, 30, 30 };
		Data.obsDoble3.hitbox = { -40, 500, 20, 80 };
		Data.obsColect3.hitbox = { -45, 570, 20, 20 };
		//---
		StopSound(Data.GameSound);
	}
}