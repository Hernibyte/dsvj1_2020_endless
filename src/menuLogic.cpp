#include"menuLogic.h"

namespace menuLog {
	void menuSet(database& Data) {

		if (Data.MenuSActivate) {
			PlaySound(Data.MenuSound);
			Data.MenuSActivate = false;
		}
		if (!IsSoundPlaying(Data.MenuSound)) {
			Data.MenuSActivate = true;
		}

		Rectangle buttom1 = { 500, 190, 300, 60 };
		Rectangle buttom2 = { 500, 290, 300, 60 };
		Rectangle buttom3 = { 500, 390, 300, 60 };

		bool buttom1check;
		bool buttom2check;
		bool buttom3check;

		buttom1check = CheckCollisionPointRec(GetMousePosition(), buttom1);
		buttom2check = CheckCollisionPointRec(GetMousePosition(), buttom2);
		buttom3check = CheckCollisionPointRec(GetMousePosition(), buttom3);

		if (buttom1check) {
			Data.menuOption = 0;
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Data.caseOption = 1;
				StopSound(Data.MenuSound);
			}
		}
		if (buttom2check) { 
			Data.menuOption = 1; 
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Data.caseOption = 2;
			}
		}
		if (buttom3check) {
			Data.menuOption = 2;
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				Data.iwn = false;
			}
		}

	}

	void OptionLog(database& Data) {
		Rectangle buttom1 = { 105, 80, 60, 40 };
		Rectangle buttom2 = { 0, 0, 0, 0 };
		Rectangle buttom3 = { 0, 0, 0, 0 };
		Rectangle buttom4 = { 0, 0, 0, 0 };
		Rectangle buttom5 = { 0, 0, 0, 0 };
		Rectangle buttom6 = { 1130, 666, 140, 40 };

		bool buttom1check;
		bool buttom2check;
		bool buttom3check;
		bool buttom4check;
		bool buttom5check;
		bool buttom6check;

		buttom1check = CheckCollisionPointRec(GetMousePosition(), buttom1);
		buttom2check = CheckCollisionPointRec(GetMousePosition(), buttom2);
		buttom3check = CheckCollisionPointRec(GetMousePosition(), buttom3);
		buttom4check = CheckCollisionPointRec(GetMousePosition(), buttom4);
		buttom5check = CheckCollisionPointRec(GetMousePosition(), buttom5);
		buttom6check = CheckCollisionPointRec(GetMousePosition(), buttom6);

		if (buttom1check) {
			Data.optionSwitch = 0;
		}
		if (buttom2check) {
			Data.optionSwitch = 1;

		}
		if (buttom3check) {
			Data.optionSwitch = 2;
			
		}
		if (buttom4check) {
			Data.optionSwitch = 3;
			
		}
		if (buttom5check) {
			Data.optionSwitch = 4;
			
		}
		if (buttom6check) {
			Data.optionSwitch = 5;
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) Data.caseOption = 0;
		}

	}
}