#ifndef MENULOGIC_H
#define MENULOGIC_H

#include"incl.h"

namespace menuLog {
	void menuSet(database& Data);
	void OptionLog(database& Data);
}

#endif // !MENULOGIC_H