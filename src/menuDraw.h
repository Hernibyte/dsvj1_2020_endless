#ifndef MENUDRAW_H
#define MENUDRAW_H

#include"incl.h"

namespace menuDr {
	void dr(database Data);
	void optDraw(database Data);
}

#endif // !MENUDRAW_H