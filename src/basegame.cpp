#include"basegame.h"

namespace bGame {

	database Database;

	void startGame() {
		init::initWin(Database);

		InitAudioDevice();

		Database.image = LoadTexture("../res/assets/fondo-principal.png");
		Database.esqueleto = LoadTexture("../res/assets/esqueleto.png");
		Database.hueso = LoadTexture("../res/assets/hueso.png");
		Database.personaje = LoadTexture("../res/assets/personaje.png");
		Database.zombie = LoadTexture("../res/assets/zombie.png");

		Database.MenuSound = LoadSound("../res/assets/MenuSound.mp3");
		Database.GameSound = LoadSound("../res/assets/GameSound.mp3");
		Database.SaltoSound = LoadSound("../res/assets/Salto.mp3");
		Database.GolpeSound = LoadSound("../res/assets/Golpe.mp3");

		while (!WindowShouldClose() && Database.iwn == true) {
			//upgrade
			upgrade::upgrad(Database);
			//draw
			draw::dr(Database);
		}

		UnloadTexture(Database.image);
		UnloadTexture(Database.esqueleto);
		UnloadTexture(Database.hueso);
		UnloadTexture(Database.personaje);
		UnloadTexture(Database.zombie);

		UnloadSound(Database.MenuSound);
		UnloadSound(Database.GameSound);
		UnloadSound(Database.SaltoSound);
		UnloadSound(Database.GolpeSound);

		CloseAudioDevice();

		CloseWindow();
	}
}